package com.example.ankit.alfrescosdkdemo;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import org.alfresco.mobile.android.api.exceptions.AlfrescoSessionException;
import org.alfresco.mobile.android.api.model.Site;
import org.alfresco.mobile.android.api.services.SiteService;
import org.alfresco.mobile.android.api.session.RepositorySession;

import java.util.ArrayList;

public class SitesActivity extends AppCompatActivity {

    private SiteArrayAdapter adapter;
    private ArrayList<Site> sites;
    private SiteService siteService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sites);

        getSupportActionBar().setTitle("Sites");

        ListView lv = (ListView) findViewById(R.id.lv_sites);

        sites = new ArrayList<>();
        adapter = new SiteArrayAdapter(this, sites);
        lv.setAdapter(adapter);

        adapter.setNotifyOnChange(true);

        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {

                Intent intent = new Intent(getApplicationContext(), SiteDetailsActivity.class);

                Site site = sites.get(position);

                intent.putExtra("targetSite", site);
                intent.putExtra("siteService", siteService);
                startActivity(intent);

            }
        });

        SharedPreferences sharedPreferences = getSharedPreferences("ALFRESCO_PREF", MODE_PRIVATE);
        String url = sharedPreferences.getString("HOSTNAME", "");
        String username = sharedPreferences.getString("USERNAME", "");
        String password = sharedPreferences.getString("PASSWORD", "");

        new ConnectToRepo().execute(url, username, password);
    }

    class ConnectToRepo extends AsyncTask<String, Integer, String> {

        private static final String TAG = "ConnectToRepo";

        @Override
        protected String doInBackground(String... params) {

            Log.d(TAG, "doInBackground");
            Log.d(TAG, params[0] + ":" + params[1] + ":" + params[2]);

            String url = params[0];
            String username = params[1];
            String password = params[2];

            try {

                // get application context
                MyApplication app = (MyApplication) getApplication();

                // connect to on-premise repo
                RepositorySession session = RepositorySession.connect(url,
                        username, password);

                // save session in application context
                app.setSession(session);

                // Get site service
                siteService = session.getServiceRegistry()
                        .getSiteService();

                // Get all sites
                sites = (ArrayList<Site>) siteService.getAllSites();


            } catch (AlfrescoSessionException e) {
                Log.e(TAG, "Failed to connect: " + e.toString());
                System.exit(0);
            }

            Log.d(TAG, "doInBackground Complete");
            return "doInBackground Complete";
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            Log.d(TAG, "onPostExecute");
            //Toast.makeText(SitesActivity.this, result, Toast.LENGTH_LONG).show();
            adapter.addSites(sites);
            adapter.notifyDataSetChanged();
        }

    }

}
