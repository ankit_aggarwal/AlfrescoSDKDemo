package com.example.ankit.alfrescosdkdemo;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import org.alfresco.mobile.android.api.model.Site;

import java.util.ArrayList;

public class SiteArrayAdapter extends ArrayAdapter<String> {

    private Context context;
    private ArrayList<Site> sites;
    private int count;

    public SiteArrayAdapter(Context context, ArrayList<Site> sites) {
        super(context, android.R.layout.simple_list_item_1, android.R.id.text1, getValues(sites));
        this.context = context;
        this.sites = sites;
        this.count = this.sites.size();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(android.R.layout.simple_list_item_1, null);
        TextView textView = (TextView) rowView.findViewById(android.R.id.text1);
        textView.setText(this.sites.get(position).getShortName());

        return rowView;
    }

    @Override
    public int getCount() {
        return this.count;
    }


    // returns list of strings that are site short names
    private static ArrayList<String> getValues(ArrayList<Site> sites) {

        ArrayList<String> values = new ArrayList<String>();

        for (Site site : sites) {
            values.add(site.getShortName());
        }

        return values;
    }

    public void addSites(ArrayList<Site> sites) {
        this.sites.addAll(sites);
        this.count = this.sites.size();
    }
}