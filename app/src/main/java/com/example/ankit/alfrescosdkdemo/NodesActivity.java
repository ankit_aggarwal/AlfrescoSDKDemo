package com.example.ankit.alfrescosdkdemo;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import org.alfresco.mobile.android.api.model.Folder;
import org.alfresco.mobile.android.api.model.Node;
import org.alfresco.mobile.android.api.model.Site;
import org.alfresco.mobile.android.api.services.DocumentFolderService;
import org.alfresco.mobile.android.api.services.SiteService;

import java.util.ArrayList;
import java.util.List;

public class NodesActivity extends AppCompatActivity {

    public final static String EXTRA_MESSAGE = "com.alfresco.tutorials.testapp1.MESSAGE";

    private NodeArrayAdapter adapter;
    private ArrayList<Node> nodes;

    Site site;
    SiteService siteService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nodes);

        // Show the Up button in the action bar.
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        ListView lv = (ListView) findViewById(R.id.lv_nodes);

        nodes = new ArrayList<>();
        adapter = new NodeArrayAdapter(this, nodes);
        lv.setAdapter(adapter);

        adapter.setNotifyOnChange(true);

        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {

                Intent intent = new Intent(getApplicationContext(), NodeDetailsActivity.class);

                Node node = nodes.get(position);

                MessageObject message = new MessageObject(node);
                intent.putExtra(EXTRA_MESSAGE, message);
                startActivity(intent);

            }
        });

        // Get message from intent
        Intent intent = getIntent();
        site = (Site) intent.getSerializableExtra("targetSite");
        siteService = intent.getParcelableExtra("siteService");

        getSupportActionBar().setTitle(site.getTitle() + " Nodes");

        new GetNodesTask().execute();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    class GetNodesTask extends AsyncTask<String, Integer, String> {

        private static final String TAG = "ConnectToRepo";
        private List<Node> nodes;

        @Override
        protected String doInBackground(String... params) {

            Log.d(TAG, "doInBackground");

            // Get site document library
            Folder folder = siteService.getDocumentLibrary(site);

            // Find DocumentFolderService
            DocumentFolderService documentFolderService = ((MyApplication) getApplication())
                    .getSession().getServiceRegistry().getDocumentFolderService();

            // Get children of document library
            nodes = documentFolderService.getChildren(folder);

            Log.d(TAG, "doInBackground Complete");
            return "doInBackground Complete";
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            Log.d(TAG, "onPostExecute");
            //Toast.makeText(NodesActivity.this, result, Toast.LENGTH_LONG).show();
            adapter.addNodes((ArrayList<Node>) nodes);
            adapter.notifyDataSetChanged();
        }

    }

}
