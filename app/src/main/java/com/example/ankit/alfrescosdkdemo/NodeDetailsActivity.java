package com.example.ankit.alfrescosdkdemo;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.widget.TextView;

import org.alfresco.mobile.android.api.model.Node;
import org.alfresco.mobile.android.api.model.Tag;
import org.alfresco.mobile.android.api.services.TaggingService;
import org.alfresco.mobile.android.api.session.RepositorySession;

import java.util.List;

public class NodeDetailsActivity extends AppCompatActivity {

    private static final String TAG = "NodeDetailActivity";
    private Node node;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_node_details);

        // Show the Up button in the action bar.
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        // Get message from intent
        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();
        MessageObject message = bundle.getParcelable(NodesActivity.EXTRA_MESSAGE);

        node = message.getNode();
        if (node == null) {
            Log.d(TAG, "FATAL ERROR: Node is null!");
            System.exit(0);
        } else {

            getSupportActionBar().setTitle(node.getTitle());

            // We now need to display node details in a UI
            ((TextView) findViewById(R.id.tv_node_name)).setText(node.getName());

            ((TextView) findViewById(R.id.tv_node_title)).setText(node.getTitle());

            ((TextView) findViewById(R.id.tv_node_description)).setText(node.getDescription());

            ((TextView) findViewById(R.id.tv_node_created_by)).setText(node.getCreatedBy());

            ((TextView) findViewById(R.id.tv_node_created_at)).setText(node.getCreatedAt().getTime().toString());

            ((TextView) findViewById(R.id.tv_node_identifier)).setText(node.getIdentifier());

            ((TextView) findViewById(R.id.tv_node_type)).setText(node.getType());

            ((TextView) findViewById(R.id.tv_node_is_folder)).setText(String.valueOf(node.isFolder()));

            ((TextView) findViewById(R.id.tv_node_is_document)).setText(String.valueOf(node.isDocument()));

            // Support for tags
            new GetTagsTask().execute();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    // get tags in background task and returns a string of tags
    class GetTagsTask extends AsyncTask<Void, Integer, String> {

        private static final String TAG = "GetTagsTask";

        @Override
        protected String doInBackground(Void... params) {

            String tagString = "";

            Log.d(TAG, "doInBackground");

            MyApplication app = (MyApplication) getApplication();
            RepositorySession session = app.getSession();
            TaggingService taggingService = session.getServiceRegistry()
                    .getTaggingService();
            List<Tag> tags = taggingService.getTags(node);

            for (Tag tag : tags) {
                tagString = tagString + tag.getValue() + ", ";
            }

            Log.d(TAG, "doInBackground Complete");
            return tagString;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            ((TextView) findViewById(R.id.tv_node_tags)).setText(result);
        }
    }
}
