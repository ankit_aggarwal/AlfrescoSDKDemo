package com.example.ankit.alfrescosdkdemo;

import android.app.Application;

import org.alfresco.mobile.android.api.session.RepositorySession;

public class MyApplication extends Application {

    private RepositorySession session;

    public void setSession(RepositorySession session) {
        this.session = session;
    }

    public RepositorySession getSession() {
        return this.session;
    }
}
