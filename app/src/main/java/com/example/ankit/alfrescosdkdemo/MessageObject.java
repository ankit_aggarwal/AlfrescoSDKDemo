package com.example.ankit.alfrescosdkdemo;

import android.os.Parcel;
import android.os.Parcelable;

import org.alfresco.mobile.android.api.model.Node;

public class MessageObject implements Parcelable {

    private Node node;
    // other information as required

    public MessageObject(Node node) {
        this.node = node;
    }

    public Node getNode() {
        return this.node;
    }

    public void setNode(Node node) {
        this.node = node;
    }

    protected MessageObject(Parcel in) {
        readFromParcel(in);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(this.node, flags);
    }

    private void readFromParcel(Parcel in) {
        this.node = in.readParcelable(Node.class.getClassLoader());
    }

    public static final Creator<MessageObject> CREATOR = new Creator<MessageObject>() {
        @Override
        public MessageObject createFromParcel(Parcel in) {
            return new MessageObject(in);
        }

        @Override
        public MessageObject[] newArray(int size) {
            return new MessageObject[size];
        }
    };
}
