package com.example.ankit.alfrescosdkdemo;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import org.alfresco.mobile.android.api.model.Node;

import java.util.ArrayList;

public class NodeArrayAdapter extends ArrayAdapter<String> {

    private Context context;
    private ArrayList<Node> nodes;
    private int count;

    public NodeArrayAdapter(Context context, ArrayList<Node> nodes) {
        super(context, android.R.layout.simple_list_item_1, android.R.id.text1, getValues(nodes));
        this.context = context;
        this.nodes = nodes;
        this.count = this.nodes.size();
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(android.R.layout.simple_list_item_1, null);
        TextView textView = (TextView) rowView.findViewById(android.R.id.text1);
        textView.setText(this.nodes.get(position).getName());

        return rowView;
    }

    @Override
    public int getCount() {
        return this.count;
    }


    // returns list of strings that are node names
    private static ArrayList<String> getValues(ArrayList<Node> nodes) {

        ArrayList<String> values = new ArrayList<>();

        for (Node node : nodes) {
            values.add(node.getName());
        }

        return values;
    }

    public void addNodes(ArrayList<Node> nodes) {
        this.nodes.addAll(nodes);
        this.count = this.nodes.size();
    }
}
