package com.example.ankit.alfrescosdkdemo;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import org.alfresco.mobile.android.api.exceptions.AlfrescoServiceException;
import org.alfresco.mobile.android.api.model.Site;
import org.alfresco.mobile.android.api.services.SiteService;

import java.util.List;

public class SiteDetailsActivity extends AppCompatActivity {

    private static final int NOP = 0;
    private static final int JOIN = 100;
    private static final int JOIN_REQUEST = 101;
    private static final int LEAVE = 102;
    private static final int CANCEL = 103;
    private static final int ADD_FAVE = 104;
    private static final int REMOVE_FAVE = 105;
    private static final int BROWSE_NODES = 106;
    private static final int FINISH = 107;

    private Site site;
    private SiteService siteService;

    private int operation = NOP;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_site_details);

        // Show the Up button in the action bar.
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        // Get message from intent
        Intent intent = getIntent();
        site = (Site) intent.getSerializableExtra("targetSite");
        siteService = intent.getParcelableExtra("siteService");

        getSupportActionBar().setTitle(site.getTitle());

        // We now need to display site details in a UI
        ((TextView) findViewById(R.id.tv_site_short_name)).setText(site.getShortName());

        ((TextView) findViewById(R.id.tv_site_description)).setText(site.getDescription());

        ((TextView) findViewById(R.id.tv_site_identifier)).setText(site.getIdentifier());

        ((TextView) findViewById(R.id.tv_site_GUID)).setText(site.getGUID());

        ((TextView) findViewById(R.id.tv_site_title)).setText(site.getTitle());

        ((TextView) findViewById(R.id.tv_site_visibility)).setText(site.getVisibility().value());

        ((TextView) findViewById(R.id.tv_site_is_favorite)).setText(String.valueOf(site.isFavorite()));

        ((TextView) findViewById(R.id.tv_site_is_member)).setText(String.valueOf(site.isMember()));

        ((TextView) findViewById(R.id.tv_site_is_pending_member)).setText(String.valueOf(site.isPendingMember()));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.menu_site_details_activity, menu);

        String visibility = site.getVisibility().value();
        boolean isMember = site.isMember();
        boolean isPendingMember = site.isPendingMember();
        boolean isFavorite = site.isFavorite();

        // if public and not member show Join
        if (visibility.equalsIgnoreCase("PUBLIC") && !isMember) {
            menu.findItem(R.id.action_join).setVisible(true);
        }

        // if member show Leave
        if (isMember) {
            menu.findItem(R.id.action_leave).setVisible(true);
        }

        // if moderated and not member show Join Request, but not if request pending
        if (visibility.equalsIgnoreCase("MODERATED") && !isMember && !isPendingMember) {
            menu.findItem(R.id.action_join_request).setVisible(true);
        }

        // if join request pending show Cancel join request
        if (isPendingMember) {
            menu.findItem(R.id.action_cancel_join_request).setVisible(true);
        }

        // if member and not favorite show Add to favorites
        if (isMember && !isFavorite) {
            menu.findItem(R.id.action_add_to_favourites).setVisible(true);
        }

        // if favorite show remove from favorites
        if (isFavorite) {
            menu.findItem(R.id.action_remove_from_favourites).setVisible(true);
        }

        // if member then show browse nodes option
        if (isMember) {
            menu.findItem(R.id.action_browse_nodes).setVisible(true);
        }

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                operation = FINISH;
                break;

            case R.id.action_join:
                operation = JOIN;
                break;

            case R.id.action_leave:
                operation = LEAVE;
                break;

            case R.id.action_join_request:
                operation = JOIN_REQUEST;
                break;

            case R.id.action_cancel_join_request:
                operation = CANCEL;
                break;

            case R.id.action_add_to_favourites:
                operation = ADD_FAVE;
                break;

            case R.id.action_remove_from_favourites:
                operation = REMOVE_FAVE;
                break;

            case R.id.action_browse_nodes:
                operation = BROWSE_NODES;
                break;

            default:
                operation = NOP;
                break;
        }

        if (operation == BROWSE_NODES) {
            Intent intent = new Intent(getApplicationContext(), NodesActivity.class);
            intent.putExtra("targetSite", site);
            intent.putExtra("siteService", siteService);
            startActivity(intent);
        } else if (operation == FINISH) {
            finish();
        } else {
            // p1 and p2 for future use. Operation is passed but not used.
            new SubmitSiteOperation().execute(String.valueOf(operation), "p1", "p2");
        }

        return super.onOptionsItemSelected(item);
    }

    class SubmitSiteOperation extends AsyncTask<String, Integer, String> {

        private static final String TAG = "SubmitSiteOperations";

        private String message;

        @Override
        protected String doInBackground(String... params) {

            Log.d(TAG, "doInBackground");
            Log.d(TAG, params[0] + ":" + params[1] + ":" + params[2]);

            try {

                switch (operation) {

                    case JOIN:
                        site = siteService.joinSite(site);
                        message = "You joined site " + site.getShortName();
                        break;

                    case LEAVE:
                        site = siteService.leaveSite(site);
                        message = "You left site " + site.getShortName();
                        break;

                    case JOIN_REQUEST:
                        site = siteService.joinSite(site);
                        message = "Request queued for site " + site.getShortName();
                        break;

                    case CANCEL:
                        // get pending site requests
                        // and cancel where shortName matches
                        List<Site> requests = siteService
                                .getPendingSites();
                        for (Site request : requests) {
                            if (request.getShortName().equals(
                                    site.getShortName())) {
                                site = siteService.cancelRequestToJoinSite(request);
                            }
                        }
                        message = "Requests cancelled for " + site.getShortName();
                        break;

                    case ADD_FAVE:
                        site = siteService.addFavoriteSite(site);
                        message = site.getShortName() + " added to your favorites";
                        break;

                    case REMOVE_FAVE:
                        site = siteService.removeFavoriteSite(site);
                        message = site.getShortName() + " removed from your favorites";
                        break;

                    case NOP:
                        message = "Nothing useful!";
                        break;
                }

            } catch (AlfrescoServiceException e) {
                Log.e(TAG,
                        "Error while working with site: " + site.getShortName());
                Log.e(TAG, "Exception generated: " + e.toString());
                Log.e(TAG, "With Error Code: " + e.getErrorCode());

                System.exit(0);
            }

            Log.d(TAG, "Operation Complete: " + operation + " " + message);
            return message;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            Log.d(TAG, "onPostExecute");
            Toast.makeText(SiteDetailsActivity.this, result, Toast.LENGTH_LONG)
                    .show();
        }

    }
}
