package com.example.ankit.alfrescosdkdemo;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class LoginActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        getSupportActionBar().setTitle("Login");

        final EditText etHostName = (EditText) findViewById(R.id.et_domain);
        final EditText etUserName = (EditText) findViewById(R.id.et_username);
        final EditText etPassword = (EditText) findViewById(R.id.et_password);
        Button btnLogin = (Button) findViewById(R.id.btn_login);

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (TextUtils.isEmpty(etHostName.getText())) {
                    etHostName.setError("Host name cannot be empty!");
                } else if (TextUtils.isEmpty(etUserName.getText())) {
                    etUserName.setError("User name cannot be empty!");
                } else if (TextUtils.isEmpty(etPassword.getText())) {
                    etPassword.setError("Password cannot be empty!");
                } else {
                    getSharedPreferences("ALFRESCO_PREF", MODE_PRIVATE).edit()
                            .putString("HOSTNAME", etHostName.getText().toString())
                            .putString("USERNAME", etUserName.getText().toString())
                            .putString("PASSWORD", etPassword.getText().toString())
                            .commit();
                    Intent intent = new Intent(LoginActivity.this, SitesActivity.class);
                    startActivity(intent);
                    finish();
                }
            }
        });
    }
}
